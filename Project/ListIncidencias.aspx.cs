﻿using System;
using System.IO;
using System.Text;
using System.Configuration;
using System.Globalization;
using System.Data;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Test
{
    public partial class ListIncidencias : System.Web.UI.Page
    {
        // Definición de variables globales
        int puesto                       // Directorio de archivo	    
        {
            get { return (int)ViewState["puesto"]; }
            set { ViewState["puesto"] = value; }
        }
        string nombre
        {
            get { return (string)ViewState["nombre"]; }
            set { ViewState["nombre"] = value; }
        }

        // Definición de variable de conexión
        string myConnectionString = "server=localhost;database=unops_test;uid=root;pwd=$Alejandro2021;";

        // Definición de variables de control
        int operacion = 0;  // Control para la modalidad del usuario

        protected void Inicializa_campos()
        {
            TB_Asunto.Text = "";
            TB_Fecha.Text = "";
            TB_Horas.Text = "";
            DD_Perfil.SelectedIndex = 0;
            DD_Estado.SelectedIndex = 0;
            DD_Prioridad.SelectedIndex = 0;
            DD_Clasificacion.SelectedIndex = 0;
        }
        protected void Inicializa_componentes()
        {
            Actualiza_GV();
            puesto = 0;
            nombre = "";
        }

        // Actualización de listado
        private void Actualiza_GV()
        {
            // Definición de controles para la manipulación y consulta de la base de datos
            MySqlConnection conn;
            conn = new MySqlConnection(myConnectionString);
            MySqlDataAdapter adaptador;
            // Definición de comando SQL para actualizar pantalla
            string sql_comando = "SELECT a.nombre_completo, b.seq_personal, b.seq_puesto, b.fecha, b.seq_clasificacion, " + 
                "b.seq_prioridad, b.asunto, b.horas, b.seq_estado, b.seq_incidencias " +
                "FROM personal a, incidencias b " +
                "WHERE a.seq_personal = b.seq_personal ORDER BY b.seq_incidencias DESC";
            // SET de tabla de datos
            conn.Open();
            DataTable dt = new DataTable();
            adaptador = new MySqlDataAdapter(sql_comando, conn);
            adaptador.Fill(dt);
            // Verificación de contenido
            if (dt.Rows.Count > 0)      // Fetch de primer registro
            {
                DD_Perfil.SelectedValue = dt.Rows[0].ItemArray[1].ToString();
                TB_Fecha.Text = dt.Rows[0].ItemArray[3].ToString();
                DD_Clasificacion.SelectedValue = dt.Rows[0].ItemArray[4].ToString();
                DD_Prioridad.SelectedValue = dt.Rows[0].ItemArray[5].ToString();
                TB_Asunto.Text = dt.Rows[0].ItemArray[6].ToString();
                TB_Horas.Text = dt.Rows[0].ItemArray[7].ToString();
                DD_Estado.SelectedValue = dt.Rows[0].ItemArray[8].ToString();
                HF_Secuencia.Value = dt.Rows[0].ItemArray[9].ToString();
                B_Agregar.Enabled = false;
                B_Nuevo.Enabled = true;
                B_Modificar.Enabled = true;
                B_Eliminar.Enabled = true;
                puesto = Convert.ToInt16(dt.Rows[0].ItemArray[2].ToString());
                DD_Perfil.Focus();
            }
            else
            {
                Inicializa_campos();
                B_Modificar.Enabled = false;
                B_Agregar.Enabled = false;
                B_Eliminar.Enabled = false;
                B_Nuevo.Enabled = true;
            }
            conn.Close();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                Inicializa_componentes();
            }
        }

        public class WebMsgBox
        {
            protected static Hashtable handlerPages = new Hashtable();
            private WebMsgBox()
            {
            }

            public static void Show(string Message)
            {
                if (!(handlerPages.Contains(HttpContext.Current.Handler)))
                {
                    Page currentPage = (Page)HttpContext.Current.Handler;
                    if (!((currentPage == null)))
                    {
                        Queue messageQueue = new Queue();
                        messageQueue.Enqueue(Message);
                        handlerPages.Add(HttpContext.Current.Handler, messageQueue);
                        currentPage.Unload += new EventHandler(CurrentPageUnload);
                    }
                }
                else
                {
                    Queue queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));
                    queue.Enqueue(Message);
                }
            }

            private static void CurrentPageUnload(object sender, EventArgs e)
            {
                Queue queue = ((Queue)(handlerPages[HttpContext.Current.Handler]));
                if (queue != null)
                {
                    StringBuilder builder = new StringBuilder();
                    int iMsgCount = queue.Count;
                    builder.Append("<script language='javascript'>");
                    string sMsg;
                    while ((iMsgCount > 0))
                    {
                        iMsgCount = iMsgCount - 1;
                        sMsg = System.Convert.ToString(queue.Dequeue());
                        sMsg = sMsg.Replace("\"", "'");
                        builder.Append("alert( \"" + sMsg + "\" );");
                    }
                    builder.Append("</script>");
                    handlerPages.Remove(HttpContext.Current.Handler);
                    HttpContext.Current.Response.Write(builder.ToString());
                }
            }
        }

        protected void B_Nuevo_Click(object sender, EventArgs e)
        {
            Inicializa_campos();
            B_Modificar.Enabled = false;
            B_Eliminar.Enabled = false;
            B_Agregar.Enabled = true;
            DD_Perfil.Focus();
        }

        protected void B_Agregar_Click(object sender, EventArgs e)
        {
            bool err = true;
            if (TB_Asunto.Text.Trim() != "" && TB_Fecha.Text.Trim() != "" && TB_Horas.Text.Trim() != "")
            {
                // Definición de controles para la manipulación y consulta de la base de datos
                MySqlConnection conn;
                MySqlCommand cmm;
                conn = new MySqlConnection(myConnectionString);
                MySqlDataAdapter adaptador;
                // Definición de comando SQL para leer puesto
                string sql_comando = "SELECT a.seq_puesto FROM puestos_personal a, personal b " +
                    "WHERE a.seq_personal = b.seq_personal AND b.seq_personal = " + DD_Perfil.SelectedValue.ToString();
                // SET de tabla de datos
                conn.Open();
                DataTable dt = new DataTable();
                adaptador = new MySqlDataAdapter(sql_comando, conn);
                adaptador.Fill(dt);
                // Verificación de contenido
                if (dt.Rows.Count > 0)
                {      // Fetch de primer registro
                    puesto = Convert.ToInt16(dt.Rows[0].ItemArray[0].ToString());
                    // Definición de comando SQL para actualizar pantalla
                    cmm = new MySqlCommand("insert into incidencias(seq_personal, seq_puesto, fecha, seq_clasificacion, seq_prioridad, asunto, horas, seq_estado)" +
                        " values(" + DD_Perfil.SelectedValue.ToString() + "," + puesto.ToString() + ",'" + TB_Fecha.Text + "'," + DD_Clasificacion.SelectedValue.ToString() +
                        "," + DD_Prioridad.SelectedValue.ToString() + ",'" + TB_Asunto.Text + "'," + TB_Horas.Text + "," + DD_Estado.SelectedValue.ToString() + ")", conn);
                    cmm.ExecuteNonQuery();
                    err = false;
                }
                conn.Close();
                if (!err) 
                { 
                    Inicializa_campos();
                    GV_Incidencias.DataBind();
                    DD_Perfil.Focus();
                    WebMsgBox.Show("El registro de incidencias ha sido agregado exitosamente"); 
                }
                else
                    WebMsgBox.Show("Hubo un error al momento de agregar en la base de datos");
                dt.Dispose();
            }
            else
            {
                WebMsgBox.Show("Faltan parámetros");
            }
        }

        protected void DD_Perfil_SelectedIndexChanged(object sender, EventArgs e)
        {
//            puesto = Convert.ToUInt16(DD_Perfil.Items[3].Value.ToString());
        }

        protected void GV_Incidencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            // SET de secuencia 
            HF_Secuencia.Value = GV_Incidencias.SelectedRow.Cells[1].Text;
            // Definición de controles para la manipulación y consulta de la base de datos
            MySqlConnection conn;
            conn = new MySqlConnection(myConnectionString);
            MySqlDataAdapter adaptador;
            // Definición de comando SQL para leer puesto
            string sql_comando = "SELECT a.nombre_completo, b.descripcion puesto, c.descripcion clasificacion, d.descripcion estado, " +
                "e.descripcion prioridad, f.seq_personal, f.seq_puesto, f.seq_clasificacion, f.seq_estado, f.seq_prioridad " +
                "FROM personal a, puestos b, clasificacion_incidencias c, estados d, prioridades e, incidencias f " +
                "WHERE a.seq_personal = f.seq_personal AND b.seq_puesto = f.seq_puesto AND c.seq_clasificacion = f.seq_clasificacion AND d.seq_estado = f.seq_estado AND " +
                "e.seq_prioridad = f.seq_prioridad AND f.seq_incidencias = " + HF_Secuencia.Value;
            // SET de tabla de datos
            conn.Open();
            DataTable dt = new DataTable();
            adaptador = new MySqlDataAdapter(sql_comando, conn);
            adaptador.Fill(dt);
            // Verificación de contenido
            if (dt.Rows.Count > 0)
            {
                puesto = Convert.ToInt16(dt.Rows[0].ItemArray[6].ToString());
                DD_Perfil.SelectedValue = dt.Rows[0].ItemArray[5].ToString();
                DD_Clasificacion.SelectedValue = dt.Rows[0].ItemArray[7].ToString();
                DD_Estado.SelectedValue = dt.Rows[0].ItemArray[8].ToString();
                DD_Prioridad.SelectedValue = dt.Rows[0].ItemArray[9].ToString();

                nombre = GV_Incidencias.SelectedRow.Cells[3].Text;
                TB_Fecha.Text = GV_Incidencias.SelectedRow.Cells[4].Text;
                TB_Asunto.Text = HttpUtility.HtmlDecode(GV_Incidencias.SelectedRow.Cells[7].Text);
                TB_Horas.Text = GV_Incidencias.SelectedRow.Cells[8].Text;
                B_Agregar.Enabled = false;
                B_Modificar.Enabled = true;
                B_Eliminar.Enabled = true;
            }
            conn.Close();
            dt.Dispose();
        }

        protected void B_Modificar_Click(object sender, EventArgs e)
        {
            DateTime dt;
            string   fd = "",       // Convierte a formato de fecha (yyyy-MM-dd)
                     dm = "";       // Verifica longitud de fecha
            bool     rs = false,    // Resultado de comando de conversión de fechas
                     er = false;    // Error
            System.Globalization.CultureInfo enUS = new System.Globalization.CultureInfo("en-US");

            // Definición de controles para la manipulación y consulta de la base de datos
            MySqlConnection conn;
            MySqlCommand cmm;
            conn = new MySqlConnection(myConnectionString);

            // Definición de comando SQL para actualizar pantalla
            if (TB_Fecha.Text.Trim() != "")
            {
                if (TB_Fecha.Text.Contains("/"))    // Verifica si contenido contiene "/"
                {
                    // Verificación de contenido de fecha
                    dm = TB_Fecha.Text.Substring(0, TB_Fecha.Text.IndexOf("/"));
                    if (dm.Length == 1)
                        TB_Fecha.Text = "0" + TB_Fecha.Text;
                    rs = DateTime.TryParseExact(TB_Fecha.Text, "dd/MM/yyyy HH:mm:ss", enUS,
                                     System.Globalization.DateTimeStyles.None,
                                     out dt);
                    // Es fecha válida (rs = true)
                    if (rs)
                    {
                        // Actualiza registro
                        fd = dt.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        cmm = new MySqlCommand("UPDATE incidencias SET seq_prioridad = " + DD_Prioridad.SelectedValue +
                            ", seq_clasificacion = " + DD_Clasificacion.SelectedValue +
                            ", seq_estado = " + DD_Estado.SelectedValue +
                            ", horas = '" + TB_Horas.Text +
                            "', asunto = '" + TB_Asunto.Text +
                            "', fecha = '" + fd +
                            "' WHERE seq_incidencias = " + HF_Secuencia.Value, conn);
                        conn.Open();
                        cmm.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                else
                {
                    er = true;
                    WebMsgBox.Show("La fecha ingresada es incorrecta");
                    TB_Fecha.Focus();
                }
            }
            else
            {
                // Actualiza registro
                cmm = new MySqlCommand("UPDATE incidencias SET seq_prioridad = " + DD_Prioridad.SelectedValue +
                    ", seq_clasificacion = " + DD_Clasificacion.SelectedValue +
                    ", seq_estado = " + DD_Estado.SelectedValue +
                    ", asunto = '" + TB_Asunto.Text +
                    " horas = '" + TB_Horas.Text +
                    "' WHERE seq_incidencias = " + HF_Secuencia.Value, conn);
                conn.Open();
                cmm.ExecuteNonQuery();
                conn.Close();

            }
            if (!er)
            {
                // Actualiza pantalla
                GV_Incidencias.DataBind();
                WebMsgBox.Show("El registro ha sido actualizado");
                DD_Perfil.Focus();
                B_Agregar.Enabled = true;
                B_Modificar.Enabled = false;
                B_Eliminar.Enabled = false;

            }
        }

        protected void B_Eliminar_Click(object sender, EventArgs e)
        {
            // Definición de controles para la manipulación y consulta de la base de datos
            MySqlConnection conn;
            MySqlCommand cmm;
            conn = new MySqlConnection(myConnectionString);
            cmm = new MySqlCommand("DELETE FROM incidencias WHERE seq_incidencias = " + HF_Secuencia.Value, conn);
            conn.Open();
            cmm.ExecuteNonQuery();
            conn.Close();
            // Actualiza pantalla
            // Actualiza pantalla
            GV_Incidencias.DataBind();
            WebMsgBox.Show("El registro ha sido eliminado");
            Inicializa_campos();
            DD_Perfil.Focus();
            B_Agregar.Enabled = true;
            B_Modificar.Enabled = false;
            B_Eliminar.Enabled = false;
        }
    }
}