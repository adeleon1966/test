﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListIncidencias.aspx.cs" Inherits="Test.ListIncidencias" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100px;
        }
        .auto-style2 {
            width: 100px;
            height: 24px;
        }
        .auto-style3 {
            height: 24px;
            width: 650px;
        }
        .auto-style5 {
            height: 24px;
            width: 442px;
        }
        .auto-style6 {
            width: 650px;
        }
        .auto-style7 {
            width: 442px;
        }
        .auto-style8 {
            width: 97%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="P_Principal" runat="server" GroupingText="Listado de incidencias" Width="818px">
                <asp:GridView ID="GV_Incidencias" runat="server" AutoGenerateColumns="False" DataSourceID="Fuente" Width="805px" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="GV_Incidencias_SelectedIndexChanged" DataKeyNames="seq_incidencias">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="seq_incidencias" HeaderText="Secuencia" InsertVisible="False" ReadOnly="True" SortExpression="seq_incidencias" />
                        <asp:BoundField DataField="Perfil" HeaderText="Perfil" SortExpression="Perfil" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
                        <asp:BoundField DataField="Clasificación" HeaderText="Clasificación" SortExpression="Clasificación" />
                        <asp:BoundField DataField="Prioridad" HeaderText="Prioridad" SortExpression="Prioridad" />
                        <asp:BoundField DataField="Asunto" HeaderText="Asunto" SortExpression="Asunto" />
                        <asp:BoundField DataField="Horas" HeaderText="Horas" SortExpression="Horas" />
                        <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </asp:Panel>
            <br />
            <asp:Panel ID="P_Detalle" runat="server" GroupingText="Detalle" Width="818px">
                <table class="auto-style8">
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="L_Perfil" runat="server" Text="Usuario"></asp:Label>
                        </td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="DD_Perfil" runat="server" DataSourceID="Personal_Puestos" DataTextField="Nombre" DataValueField="seq_personal" Height="20px" Width="432px" OnSelectedIndexChanged="DD_Perfil_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style6">
                            <asp:SqlDataSource ID="Personal_Puestos" runat="server" ConnectionString="<%$ ConnectionStrings:unops_testConnectionString %>" ProviderName="<%$ ConnectionStrings:unops_testConnectionString.ProviderName %>" SelectCommand="SELECT CONCAT(c.nombre_completo,&quot; (&quot;, b.descripcion,&quot;)&quot;) Nombre, b.descripcion, a.* FROM puestos_personal a, puestos b, personal c WHERE a.seq_puesto = b.seq_puesto AND a.seq_personal = c.seq_personal "></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="L_Fecha" runat="server" Text="Fecha"></asp:Label>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="TB_Fecha" runat="server" Width="132px"></asp:TextBox>
                        </td>
                        <td class="auto-style6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style2">
                            <asp:Label ID="L_Clasificacion" runat="server" Text="Clasificación"></asp:Label>
                        </td>
                        <td class="auto-style5">
                            <asp:DropDownList ID="DD_Clasificacion" runat="server" DataSourceID="Clasificaciones" DataTextField="descripcion" DataValueField="seq_clasificacion" Height="20px" Width="119px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style3">
                            <asp:SqlDataSource ID="Clasificaciones" runat="server" ConnectionString="<%$ ConnectionStrings:unops_testConnectionString %>" ProviderName="<%$ ConnectionStrings:unops_testConnectionString.ProviderName %>" SelectCommand="SELECT seq_clasificacion, descripcion FROM clasificacion_incidencias"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="L_Prioridad" runat="server" Text="Prioridad"></asp:Label>
                        </td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="DD_Prioridad" runat="server" DataSourceID="Prioridades" DataTextField="descripcion" DataValueField="seq_prioridad" Height="16px" Width="118px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style6">
                            <asp:SqlDataSource ID="Prioridades" runat="server" ConnectionString="<%$ ConnectionStrings:unops_testConnectionString %>" ProviderName="<%$ ConnectionStrings:unops_testConnectionString.ProviderName %>" SelectCommand="SELECT seq_prioridad, descripcion FROM prioridades"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="L_Asunto" runat="server" Text="Asunto"></asp:Label>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="TB_Asunto" runat="server" Columns="1" Height="37px" TextMode="MultiLine" Width="429px"></asp:TextBox>
                        </td>
                        <td class="auto-style6">
                            <asp:SqlDataSource ID="Fuente" runat="server" ConnectionString="<%$ ConnectionStrings:unops_testConnectionString %>" ProviderName="<%$ ConnectionStrings:unops_testConnectionString.ProviderName %>" SelectCommand="SELECT b.descripcion AS Perfil, c.nombre_completo AS Nombre, a.fecha AS Fecha, d.descripcion AS Clasificación, e.descripcion AS Prioridad, a.asunto AS Asunto, a.horas AS Horas, f.descripcion AS Estado, a.seq_incidencias FROM incidencias a INNER JOIN puestos b ON a.seq_puesto = b.seq_puesto INNER JOIN personal c ON a.seq_personal = c.seq_personal INNER JOIN clasificacion_incidencias d ON a.seq_clasificacion = d.seq_clasificacion INNER JOIN prioridades e ON a.seq_prioridad = e.seq_prioridad INNER JOIN estados f ON a.seq_estado = f.seq_estado ORDER BY a.seq_incidencias DESC"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="L_Horas" runat="server" Text="Horas"></asp:Label>
                        </td>
                        <td class="auto-style7">
                            <asp:TextBox ID="TB_Horas" runat="server"></asp:TextBox>
                        </td>
                        <td class="auto-style6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="L_Estado" runat="server" Text="Estado"></asp:Label>
                        </td>
                        <td class="auto-style7">
                            <asp:DropDownList ID="DD_Estado" runat="server" DataSourceID="Estados" DataTextField="descripcion" DataValueField="seq_estado" Height="16px" Width="120px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style6">
                            <asp:SqlDataSource ID="Estados" runat="server" ConnectionString="<%$ ConnectionStrings:unops_testConnectionString %>" ProviderName="<%$ ConnectionStrings:unops_testConnectionString.ProviderName %>" SelectCommand="SELECT seq_estado, descripcion FROM estados"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">&nbsp;</td>
                        <td class="auto-style7">
                            <asp:Button ID="B_Nuevo" runat="server" OnClick="B_Nuevo_Click" Text="Nuevo" />
                            &nbsp;<asp:Button ID="B_Agregar" runat="server" Text="Agregar" Width="76px" OnClick="B_Agregar_Click" />
                            &nbsp;<asp:Button ID="B_Modificar" runat="server" Text="Modificar" Width="71px" OnClick="B_Modificar_Click" />
                            &nbsp;<asp:Button ID="B_Eliminar" runat="server" Text="Eliminar" OnClick="B_Eliminar_Click" />
                            &nbsp;</td>
                        <td class="auto-style6">
                            <asp:HiddenField ID="HF_Secuencia" runat="server" Visible="False" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <br />
            <br />
        </div>
    </form>
</body>
</html>
